const express = require('express')
const app = express() 
const transaksiRoutes = require('./routes/transaksiRoutes.js') 
const barangRoutes = require('./routes/barangRoutes.js')
const pelangganRoutes = require('./routes/pelangganRoutes.js')
const pemasokRoutes = require('./routes/pemasokRoutes.js')

app.use(express.urlencoded({extended:false})) 

app.use('/transaksi', transaksiRoutes) 
app.use('/barang', barangRoutes)
app.use('/pelanggan', pelangganRoutes)
app.use('/pemasok', pemasokRoutes)

app.listen(6666) 