// Express
const express = require('express');
const app = express();
const bodyParser = require('body-parser'); //post body handler
const transaksiRoutes = require('./routes/transaksiRoutes.js')
const barangRoutes = require('./routes/barangRoutes.js')
const pemasokRoutes = require('./routes/pemasokRoutes.js')
const pelangganRoutes = require('./routes/pelangganRoutes.js')

//Set body parser for HTTP post operation
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
  extended: true
})); // support encoded bodies

//set static assets to public directory
app.use(express.static('public'));

app.use('/transaksi', transaksiRoutes) // if accessing localhost:3000/transaksi/* we will go to transaksiRoutes
app.use('/barang', barangRoutes)
app.use('/pemasok', pemasokRoutes)
app.use('/pelanggan', pelangganRoutes)

app.listen(6666, () => console.log("server running on http://localhost:6666"))