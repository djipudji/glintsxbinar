const { Barang, Pemasok } = require("../../models")
const { check, validationResult, matchedData, sanitize } = require('express-validator')

const multer = require('multer')
const path = require('path')
const crypto = require('crypto')

const uploadDir = '/img/';
const storage = multer.diskStorage({
  destination: "./public" + uploadDir,
  filename: function(req, file, cb) {
    crypto.pseudoRandomBytes(16, function(err, raw) {
      if (err) return cb(err)

      cb(null, raw.toString('hex') + path.extname(file.originalname))
    })
  }
})

const upload = multer({ storage: storage, dest: uploadDir})
module.exports = {
    create: [

        upload.single('image'),

        check('id_pemasok').isNumeric().custom(value => {
            return Pemasok.findOne({
                where: {
                    id: value
                }
            }).then(p=> {
                if(!p) {
                    throw new Error('ID Pemasok tidak ada!')
                }
            })
        }),

        check('nama').isString().notEmpty(),

        check('harga').isNumeric(),
        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        },
    ],
    update: [

        upload.single('image'),

        check('id_pemasok').isNumeric().custom(value => {
            return Pemasok.findOne({
                where: {
                    id: value
                }
            }).then(p => {
                if(!p) {
                    throw new Error('ID pemasok tidak ada!')
                }
            })
        }),

        check('nama').isString().notEmpty(),

        check('harga').isNumeric(),
        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: erros.mapped()
                })
            }
            next();
        },
    ],
}