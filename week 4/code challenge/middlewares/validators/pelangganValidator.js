const { Pelanggan } = require("../../models")
const { check, validationResult, matchedData, sanitize } = require('express-validator')

module.exports = {
    create: [

        check('nama').isString().notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next();
        },
    ],
    update: [
        
        check('nama').isString().notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next();
        },
    ],
}