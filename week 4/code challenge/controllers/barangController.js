const { Barang, Pemasok } = require("../models")
const { check, validationResult, matchedData, sanitize } = require('express-validator')

class BarangController {

    constructor() {
        Pemasok.hasMany(Barang, {
            foreignKey: 'id_pemasok'
        })
        Barang.belongsTo(Pemasok, {
            foreignKey: 'id_pemasok'
        })
    }

    async getAll(req, res) {
        Barang.findAll({
            attributes: ['id', 'nama', 'harga', 'image', ['createdAt', 'waktu']],
            include: [{
                model: Pemasok,
                attributes: [['nama', 'nama_pemasok']]
            }]
        }).then(barang => {
            res.json(barang)
        })
    }

    async getOne(req, res) {
        Barang.findOne({
            where: {
                id: req.params.id
            },
            attributes: ['id', 'nama', 'harga', 'image', ['createdAt', 'waktu']],
            include: [{
                model: Pemasok,
                attributes: [['nama', 'nama_pemasok']]
            }]
        }).then(barang => {
            res.json(barang)
        })
    }

    async create(req, res) {
        Barang.create({
            id_pemasok: req.body.id_pemasok,
            nama: req.body.nama,
            harga: req.body.harga,
            image: req.file === undefined ? "" : req.file.filename
        }).then(newBarang => {
            res.json({
                "status": "success!",
                "message": "new barang added to the database",
                "data": newBarang
            })
        })
    }

    async update(req, res) {
        var update = {
            id_pemasok: req.body.id_pemasok,
            nama: req.body.nama,
            harga: req.body.harga,
            image: req.file === undefined ? "" : req.file.filename
        }

        Barang.update(update, {
            where: {
                id: req.params.id
            }
        }).then(affectedRow => {
            return Barang.findOne({
                where: {
                    id: req.params.id
                }
            })
        }).then(b => {
            res.json({
                "status": "success!",
                "message": "barang updated in the database",
                "data": b
            })
        })
    }

    async delete(req, res) {
        Barang.destroy({
            where: {
                id: req.params.id
            }
        }).then(affectedRow => {
            if (affectedRow) {
                return {
                    "status": "success!",
                    "message": "barang deleted in the database",
                    "data": null
                }
            }

            return {
                "status": "error",
                "message": "failed to delete barang",
                "data": null
            }
        }).then(r => {
            res.json(r)
        })
    }
}

module.exports = new BarangController;