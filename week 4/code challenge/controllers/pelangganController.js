const { Pelanggan } = require("../models")
const { check, validationResult, matchedData, sanitize } = require('express-validator')

class PelangganController {

    async getAll(req, res) {
        Pelanggan.findAll({
            attributes: ['id', 'nama', ['createdAt', 'waktu']]
        }).then(pelanggan => {
            res.json(pelanggan)
        })
    }

    async getOne(req, res) {
        Pelanggan.findOne({
            where: {
                id: req.params.id
            },
            attributes: ['id', 'nama', ['createdAt', 'waktu']]
        }).then(pelanggan => {
            res.json(pelanggan)
        })
    }

    async create(req, res) {
        Pelanggan.create({
            nama: req.body.nama
        }).then(newPelanggan => {
            res.json({
                "status": "success!",
                "message": "new pelanggan added to the database",
                "data": newPelanggan
            })
        })
    }

    async update(req, res) {
        var update = {
            nama: req.body.nama
        }

        Pelanggan.update(update, {
            where: {
                id: req.params.id
            }
        }).then(affectedRow => {
            return Pelanggan.findOne({
                where: {
                    id: req.params.id
                }
            })
        }).then(p => {
            res.json({
                "status": "success!",
                "message": "pelanggan updated in the database",
                "data": p
            })
        })
    }

    async delete(req, res) {
        Pelanggan.destroy({
            where: {
                id: req.params.id
            }
        }).then(affectedRow => {
            if(affectedRow) {
                return {
                    "status": "success!",
                    "message": "pelanggan deleted in the database",
                    "data": null
                }
            }

            return {
                "status": "error",
                "message": "failed to delete pelanggan",
                "data": null
            }
        }).then(r => {
            res.json(r)
        })
    }
}

module.exports = new PelangganController