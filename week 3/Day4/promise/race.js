const fs = require('fs')

const readFile = options => file => new Promise((resolve, reject) => {
    fs.readFile(file, options, (err, content) => {
      if (err) return reject(err)
      return resolve(content)
    })
  })
  
  const writeFile = (file, content) => new Promise((resolve, reject) => {
    fs.writeFile(file, content, err => {
      if (err) return reject(err)
      return resolve()
    })
  })
  
  const
    read = readFile('utf-8')
  
  
  Promise.race([read('1.txt'), read('2.txt'), read('3.txt'), read('4.txt')])
    .then((value) => {
      console.log(value);
    })
    .catch(error => {
      console.log(error);
    })

  