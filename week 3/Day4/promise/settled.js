const fs = require('fs')


const readFile = options => file => new Promise((resolve, reject) => {
  fs.readFile(file, options, (err, content) => {
    if (err) return reject(err)
    return resolve(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})

const
  read = readFile('utf-8'),
  files = ['1.txt', '2.txt', '3.txt', '4.txt']


Promise.allSettled(files.map(file => read(`${file}`)))
  .then(results => {

    console.log(results)
  })
