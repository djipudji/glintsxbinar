const fs = require('fs')

const readFile = options => file => new Promise((resolve, reject) => {
  fs.readFile(file, options, (err, content) => 
  {if (err) return reject(err)
    return resolve(content)}
    )
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()}
    )
})



const
  read = readFile('utf-8')

async function mergedContent() {
  try {
    
    const result = await Promise.all([
      read('1.txt'),
      read('2.txt'),
      read('3.txt'),
      read('4.txt')
    ])
    
    await writeFile('lirik.txt', result.join(' '))
  } catch (e) {
    throw e
  }

  
  return read('lirik.txt')
}

mergedContent()
  .then(result => {
    console.log(result) 
  }).catch(err => {
    console.log('Error to read/write file, error: ', err)
  }).finally(() => {
    console.log('Uyeeee!!');
  })

