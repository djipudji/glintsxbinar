
const express = require('express') 
const router = express.Router() 
const ikanController = require('../controllers/ikanController.js') 

router.get('/', ikanController.ikan)

module.exports = router; // 
