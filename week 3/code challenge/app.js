
const express = require('express') 
const app = express()
const ikanRoutes = require('./routes/ikanRoutes.js') 
app.use(express.static('public')); 


app.use('/ikan', ikanRoutes)

app.listen(8080) 