const mongoose = require('mongoose')
const mongoose_delete = require('mongoose-delete')

const BarangSchema = new mongoose.Schema({
    nama: {
        type: String,
        required: true
    },
    harga: {
        type: mongoose.Schema.Types.Decimal128,
        required: true
    },
    pemasok: {
        type: mongoose.Schema.Types.Mixed,
        required: true
    },
    image: {
        type: String,
        required: false,
        default: null
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false // Disable version column (for checking how many times we're updating the data)
})

BarangSchema.plugin(mongoose_delete, {overrideMethods: 'all' }) // Enabling soft delete

module.exports = barang = mongoose.model('barang', BarangSchema, 'barang') // AS NAME