const exppress = require('express')
const app = exppress()
const bodyParser = require('body-parser')
const transaksiRoutes= require('./transaksiRoutes')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('/transaksi',transaksiRoutes)
app.listen(6666)

