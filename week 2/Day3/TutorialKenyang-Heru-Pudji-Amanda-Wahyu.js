/* Tutorial Kenyang by : Heru, Pudji, Amanda, Wahyu */
let storage = [`Telur`, `Kentang`, `Sayuran`, `Indomie`]

function checkMieAvailibility() {
    console.clear();
    console.log("Ada ga ya mie instant nih...\n");


    for (let i=0; i < storage.length; i++) {
        if (storage[i] == `Indomie` || storage[i] == `Supermie`) {
            console.log(`Wah ada mie instant, enak nih`);
            return false;
        }
    }
    console.log(`Wah ga ada mie instant nih, pesen grabfood aja kali ya`);
    return true;
}
function pesenGrab(grabfood) {
    if (grabfood) {
        console.log(`Mesen makan lewat aplikasi grabfood`);
        console.log(`Pilih makanan yang ingin dipesan`);
        console.log(`Order makanan yang ingin dipesan`);
        tungguMakanan();
    } else {
        masakMie()
    }
}
function tungguMakanan() {
    console.log(`Tunggu makanan yang sudah dipesan`);
    console.log(`Setelah pesanan datang, buka bungkusan lalu hidangkan makanan di piring`);
    makananTersedia();
}
function makananTersedia() {
    console.log(`udah ada nih makanannya`);
    console.log(`yuk mari makan`);
    console.log(`duh kenyang gila...`);
}
function masakMie() {
    console.log(`siapkan mie instant dan alat masak yang dibutuhkan`);
    console.log(`panaskan air dalam panci/wajan/penggorengan bebas lah pokoknya`);
    console.log(`masukan mie instant ke dalam air yang sedang dipanaskan`);
    console.log(`sambil menunggu, siapkan bumbu dan taruh di piring/mangkok bebas lah`);
    console.log(`setelah matang, angkat mie instant langsung tiriskan`);
    console.log(`taruh mie instant yang sudah matang di piring/mangkuk lalu di aduk dengan bumbu sampai merata`);
    makananTersedia()
}

function aduhLaper() {
    let mauMakan = checkMieAvailibility();
    pesenGrab(mauMakan);
}

aduhLaper();