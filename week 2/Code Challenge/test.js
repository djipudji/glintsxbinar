module.exports = (sortAscending, sortDescending, data) => {
  if (process.argv.slice(2)[0] == "test") {
    try {
      let error = []

      let asc = sortAscending(data);
      for (let i = 0; i < asc.length; i++) {
        if (asc[i] > asc[i + 1]) {
          error.push(sortAscending);
          break;
        }
      }

      let dsc = sortDescending(data);
      for (let i = 0; i < asc.length; i++) {
        if (dsc[i] < dsc[i + 1]) {
          error.push(sortDescending);
          break;
        }
      }

      if (error.length > 0) {
        console.log("Result:", error[0].name == "sortAscending" ? asc : dsc);
        console.log();
        throw new Error(`${error[0].name} goblog anying`);
      } else {
        console.log("INI YANG Ascending:", asc);
        console.log("INI YANG Descending:", dsc);
      }
    }

    catch(err) {
      console.error(err.message);
    }
  }
}
