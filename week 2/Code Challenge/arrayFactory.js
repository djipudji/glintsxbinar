const data = [];
const randomNumber = Math.floor(Math.random() * 100);

function createArray() {
  for (let i = 0; i < randomNumber; i++) {
    data.push(createArrayElement())
  }

  if (data.length == 0) {
    createArray();
  }
}

function createArrayElement() {
  let random = Math.floor(Math.random() * 1000);
  return [null, random][Math.floor(Math.random() * 2)];
  clean(data);
}
function clean(data) {
  return data.filter(aw => aw != null)
}
createArray();

module.exports = data;
