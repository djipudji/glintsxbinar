const readline = require('readline')
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const kubus = require('./kubus.js');
const tabung = require('./tabung.js');
const prisma = require('./prisma.js')

function menu() {
  console.log(`Menu`);
  console.log(`====`);
  console.log(`1. Kubus`);
  console.log(`2. Tabung`);
  console.log(`3. Prisma`);
  console.log(`4. Exit`);
  rl.question(`Choose option: `, option => {
    if (!isNaN(option)) {
      if (option == 1) {
        kubus.input()
      } else if (option == 2) {
        tabung.input()
      } else if (option == 3) {
        prisma.input()
      } else if (option == 4) {
        rl.close()
      } else {
        console.log(`Option harus 1 - 4!\n`);
        menu()
      }
    } else {
      console.log(`Option harus sebuah nomor!\n`);
      menu()
    }
  })
}

menu()


module.exports.rl = rl