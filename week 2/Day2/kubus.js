const index = require('./index.js')

function calculateVolumeKubus(panjang) {
    var alaskubus = panjang * panjang;
    return alaskubus * panjang;
}
function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}
function inputPanjang() {
    index.rl.question(`Panjang: `, panjang => {
        if (!isNaN(panjang) && !isEmptyOrSpaces(panjang)) {
            console.log(`Volume kubus nya adalah ${calculateVolumeKubus(panjang)}`);
            index.rl.close();
        } else {
            console.log(`Panjang harus sebuah nomor!\n`);
            inputPanjang()
        }
    })
}    
module.exports.input = inputPanjang