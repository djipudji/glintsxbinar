const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const transaksiRoutes = require('./routes/transaksiRoutes.js')
const barangRoutes = require('./routes/barangRoutes.js')
const pemasokRoutes = require('./routes/pemasokRoutes.js')
const pelangganRoutes = require('./routes/pelangganRoutes.js')
const userRoutes = require('./routes/userRoutes');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

app.use(express.static('public'))

app.use('/', userRoutes);
app.use('/transaksi', transaksiRoutes)
app.use('/barang', barangRoutes)
app.use('/pemasok', pemasokRoutes)
app.use('/pelanggan', pelangganRoutes)

app.listen(5555, () => console.log('Server running on localhost:5555'))