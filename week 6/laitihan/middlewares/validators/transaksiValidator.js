const {
    barang,
    pelanggan,
    transaksi
} = require ('../../models/mongodb')

const{
    check,
    validationResult,
    matchedData,
    sanitize,
} = require ('express-validator');
const { Error } = require('mongoose');

module.exports = {
    detOne: [
        check('id').custom(value =>{
            return transaksi.findOne({
                _id: value
            }).then (result =>{
                if(!result){
                    throw new Error ('ID ini tidak ada')
                }})
        }),
        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()){
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        },
    ],
    create: [
        check('id_barang').custom(value=>{
            return barang.findOne({
                _id:value
            }).then(b =>{
                if(!b){
                    throw new Error('ID barang tidak ada');
                }
            })
        }),
        check('id_pelanggan').custom(value=> {
            return pelanggan.findOne({
                _id:value
            }).then(p=>
                { if (!p){
                    throw new Error('id pelanggan tidak ada!');
                }
            })
        }),
        check('jumlah').isNumeric(),
        (req,res,next)=>{
            const errrors = validationResult(req);
            if(!errors.isEmpty()){
                return res.status(422).json({
                    errrors : errors.mapped()
                });
            }
            next();
        },
    ],
    delete: [
        check('id').custom(value=>{
            return transaksi.findOne({
                _id: value
            }).then(result=> {
                if(!result){
                    throw new Error('Id transaksi tidak ada!')
                }
            })
        }),
        (req,res,next)=>{
            const errors = validationResult(req);
            if (!errors.isEmpty()){
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ]
};