const express = require ('express');
const router = express.router();
const transaksiValidator = ('../middlewares/validators/transaksiValidator');
const transaksiController = ('../controllers/transaksiControllers');

router.get('/', transaksiController.getAll);
router.get('/:id', transaksiValidator.getOne, transaksiController.getOne);
router.post('/create', transaksiValidator.post, transaksiController.post);
router.put('/update/:id', transaksiValidator.put, transaksiController.put);
router.delete('/delete/:id', transaksiValidator.delete, transaksiController.delete);

module.exports = router;