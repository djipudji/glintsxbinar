const express = require ('express')
const app = express()
const bodyParser = require('body-parser')
// const transaksiRoutes = require('.routes/transaksiRoutes');
// const barangRoutes = require('.routes/barangRoutes');
// const pemasokRoutes = require('.routes/pemasokRoutes');
// const pelangganRoutes = require('.routes/pelangganRoutes');
const userRoutes = require('./routes/userRoutes');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended:true
}));

app.use(express.static('public'));
app.use('/',userRoutes);
// app.use('/transaksi', transaksiRoutes);
// app.use('/pelanggan', pelangganRoutes);
// app.use('/pemasok', pemasokRoutes);
// app.use('/barang', barangRoutes);

app.listen(4444, ()=> console.log('server running di localhost:4444'))