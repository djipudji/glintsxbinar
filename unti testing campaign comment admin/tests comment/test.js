let mongoose = require('mongoose')
let { comment } = require('../models')
let chai = require('chai')
let chaiHttp = require('chai-http')
let server = require('../index.js')
let should = chai.should()
let fs = require('fs')
let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiMmQwZGMzN2YtOTkyOS00YjY3LWI5ZmUtYzM0MWZhZjhmM2JlIiwiZW1haWwiOiJwdWRqaUBleGFtLmNvbSJ9LCJpYXQiOjE2MTI1Mzg5NTd9.Va6lBBSmtRKuZdNA6x7V0iuAk7ewODWSRQ57UOurtpw'
let admin = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiZTVkZjhiNDYtYmQzYS00MWNmLTk4MzYtYzIzYzNkNzIyNmQ3IiwiZW1haWwiOiJwdWRqaUBlbWFpbC5jb20iLCJyb2xlIjoiYWRtaW4ifSwiaWF0IjoxNjEyNTUxMTU5fQ.v9fsH0eejNQg6TTL6HCylv8DJ-lY5-Ee82DxNEX9GXs'
let comment_id = '601edac033cc282ac524ade6'

chai.use(chaiHttp)

describe('COMMENT API', () => {

    describe('/GET all comment', () => {
        it('It should get all comment', (done) => {
            chai.request(server)
                .get('/comment/')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('Success get all the comment!');
                    res.body.should.have.property('data');
                    res.body.data.should.be.an('array')
                    res.body.should.have.property('totalPages')
                    res.body.should.have.property('currentPage')
                    done()
                })
        })
    })

    describe('/GET ONE COMMENT', () => {
        it('It should get one comment', (done) => {
            chai.request(server)
                .get('/comment/get' )
                .query({
                    comment_id : "601ed3ee63987b269320cbd0"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Success get one comment!')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    done()
                })
        })
    })

    describe('/GET COMMENT USER', () => {
        it('It should get comment user', (done) => {
            chai.request(server)
                .get('/comment/get/user' )
                .query({
                    user_id : "2d0dc37f-9929-4b67-b9fe-c341faf8f3be",
                    page : 1,
                    limit : 1
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Success get comment by user!')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('array')
                    done()
                })
        })
    })

    describe('/GET COMMENT USER', () => {
        it('It should get comment user', (done) => {
            chai.request(server)
                .get('/comment/get/campaign' )
                .query({
                    campaign_id : "600f0ce7e8a472515256771c",
                    page : 1,
                    limit : 1
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Success get comment by campaign!')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('array')
                    done()
                })
        })
    })

    describe('/CREATE COMMENT ', () => {
        it('It should create comment', (done) => {
            chai.request(server)
                .post('/comment/create/' )
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                comment : "semoga cepat sembuh",
                campaign_id : "600f0ce7e8a472515256771c"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Success create a comment!')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    done()
                })
        })
    })

    describe('/UPDATE COMMENT ', () => {
        it('It should update comment', (done) => {
            chai.request(server)
                .put('/comment/update/' + comment_id )
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                comment : "amin"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Success update a comment!')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    done()
                })
        })
    })

    describe('/UPDATE user', () => {
        it('It should update user', (done) => {
            chai.request(server)
                .put('/comment/update/user/profile')
                .set({
                    Authorization: `Bearer ${token}`
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Success update user also in comment!')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    done()
                })
        })
    })

    describe('/delete comment', () => {
        it('It should delete comment', (done) => {
            chai.request(server)
                .delete('/comment/delete')
                .set({
                    Authorization: `Bearer ${token}`
                })
                .query({
                    comment_id : '601eddf6a89c602c442a5ef0'
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Success delete a comment!')
                    res.body.should.have.property('data').eql(null)
                    done()
                })
        })
    })

    
})

